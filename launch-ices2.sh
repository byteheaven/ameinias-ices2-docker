#!/bin/bash


exec docker run \
    --rm -it \
    --name ameinias-ices2 \
    --link ameinias-icecast2:icecastserver \
    --volumes-from ameinias \
    tekhedd/ameinias-ices2 \
    "$@"
